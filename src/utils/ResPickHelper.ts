import * as vscode from 'vscode';

export default class ResPickHelper {

    public static async pickFile(canSelectMany = false,
                          filters?: { [name: string]: string[] },
                          uri?: vscode.Uri): Promise<vscode.Uri[] | undefined> {
        let options: vscode.OpenDialogOptions = {
            canSelectFiles: true,
            canSelectFolders: false,
            canSelectMany: canSelectMany,
            defaultUri: uri,
            filters: filters,
        };
        return vscode.window.showOpenDialog(options);
    }

}
