# LVGL gui designer for Aiinone-IDE

>**中文详情**

该插件由“领头羊”团队联合Aiinone-IDE打造的LVGL开源UI设计工具，传承LVGL开源之精神，期望更多志同道合的开发者来共创、共享、共赢。

#### 特别感谢

1. 感谢全志代理商深圳市汉欣诺电子有限公司( http://www.hixino.com.cn )提供F1C200/F1C100/F133等开发平台
2. 感谢kaiakz大佬提供的开源基础版本和思路( https://github.com/kaiakz/walv )
3. 感谢开发过程中提供帮忙的热心朋友

#### 插件主要功能

- 支持拖拽修改控件位置
- 支持修改控件风格和属性
- 支持生成/导出C和MicroPython代码
- 支持多页面
- 支持页面保存和恢复
- 支持配置编译/下载脚本
- 支持修改分辨率

#### 插件主界面区域介绍

![](https://gitee.com/cdaozhi/lty-plugin-lvgl/raw/master/resources/block.png)

- 1区域-工具栏：根据布局生成/导出指定类型代码；根据设置脚本，编译/下载代码
- 2区域-控件列表：添加/删除控件
- 3区域-控件层级列表：展示控件布局层级关系
- 4区域-布局编辑区：拖拽控件调整控件位置；预览生成代码
- 5区域-控件属性编辑区：编辑控件风格和相关属性
- 6区域-模拟器终端
- 7区域-分辨率编辑区：根据实际设备设置对应的分辨率

#### 插件使用方法

1. 下载和安装Aiinone-IDE和Inone-SDK，请参考[文档](https://www.aiinone.cn/developer/aiinoneide/ide/install.html)
2. 新建工程。请参考[新建工程文档](https://www.aiinone.cn/developer/aiinoneide/ide/create_project.html)，`应用类型`选择**LVGL**
3. 打开layout目录下的布局文件(*.lty)
4. 添加控件。从`2区域-控件列表`添加控件到`4区域-布局编辑区`
5. 点击`Export`导出根据布局生成的代码
6. 编译。请参考[文档](https://www.aiinone.cn/developer/aiinoneide/ide/build_project.html)
7. 下载。请参考[文档](https://www.aiinone.cn/developer/aiinoneide/ide/download.html)

#### 插件编译方法

1. 安装相关环境，请参考[文档](https://code.visualstudio.com/api/get-started/your-first-extension)
2. 在终端执行 `build.sh pack`生成vsix插件包

#### 仓库地址

https://gitee.com/cdaozhi/lty-plugin-lvgl