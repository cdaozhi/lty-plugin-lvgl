const InputType = {
    Input: 0,
    ColorSelector: 1,
    OptionSelector: 3
}

const Categorize = {
    Other: 0,
    Base: 1
}

const Data_Changed_Event = 'data_changed_event'
const MSG_AUTO_SAVE_PAGE_SUCC = 'Auto save successfully'
const MSG_SAVE_PAGE_SUCC = 'Save successfully'