
var vm = null;

function sendMessage(cmd, data) {
    return axios.post(`/index`, {cmd, data}).catch(error => {
        console.error(error);
    });
}

window.startRunning = async function() {
    let res = await sendMessage(CMD.getPageData);
    let pageData  = res.data;
    res = await sendMessage(CMD.getActFileName);
    let actFileName = res.data;

    vm = new Vue(WALV_MAIN);
    vm.act_FileName = actFileName;

    /* Initialize the wasm mpy */
    mpylvInit(vm, pageData);

    /* Restore page data */
    restorePageData(vm, pageData);

    /* Initialize the ace editor */
    editorInit(vm);

    document.title = "LTY: The Online Designer For LittlevGL";
}

const restorePageData = (vm, pageData) => {
    if (!pageData) {
        return;
    }

    vm.screenWidth = pageData?.WidgetPool?.screen?.width;
    vm.screenHeight = pageData?.WidgetPool?.screen?.height;
    Object.assign(vm.$data, pageData);

    for(let key in pageData.InfoPool) {
        if (key === 'screen') {continue;}
        wrap_create(key, pageData.InfoPool[key].parent, pageData.InfoPool[key].type, false);
        wrap_attributes_setter_str(key, pageData.InfoPool, pageData.WidgetPool);
        wrap_style_setter_str(key, pageData.InfoPool, pageData.WidgetPool);
        wrap_apis_setter_str(key, pageData.InfoPool, pageData.WidgetPool);
    }
    vm.selectScreenTreeNode();
};


const mpylvInit = (vm, pageData) => {

    Module.canvas = document.getElementById("canvas");

    /* Bind mp_js_stdout */
    mp_js_stdout = document.getElementById('mp_js_stdout');
    mp_js_stdout.value = "";

    /* Initialize the xtermjs */
    Terminal.applyAddon(fit);
    var term = new Terminal({
      cursorBlink: true,
    //   theme: {
    //     background: '#fdf6e3'
    //   }
    });
    term.open(document.getElementById("mpy_repl"), true);
    term.fit();
    term.write('Welcome To \x1B[1;3;31mLTY LVGL-Extension\x1B[0m');

    /*Initialize MicroPython itself*/
    mp_js_init(8 * 1024 * 1024);

    /*Setup printing event handler*/
    // mp_js_stdout.addEventListener('python:stdout_print', function(e) {
    //     console.log(e.data);
    //     term.write(vm.handleOutput(e.data));
    // }, false);

    window.addEventListener("python:stdout_print", (e) => {
        setTimeout(() => {
            for (let i = 0; i < e.data.length; i++) {
                term.write(vm.handleOutput(e.data[i]));
            }
        }, 50)
    }, false);

    window.addEventListener(Data_Changed_Event, (e) => {
        debounceFun(10 * 1000, function (v, v2) {
            vm.savePage({}, MSG_AUTO_SAVE_PAGE_SUCC);
        })
    }, false)

    /*Setup key input handler */
    term.on('data', function(key, e) {
        for(var i = 0; i < key.length; i++) {
            mp_js_process_char(key.charCodeAt(i));
        }
    });

    vm.Term = term;
    const width = pageData?.WidgetPool?.screen?.width || vm.screenWidth;
    const height = pageData?.WidgetPool?.screen?.height || vm.screenHeight;

    /* Run init script */
    mp_js_do_str(EnvInitCode(width, height).join('\n'));

    /* Add function querry_attr() & walv_callback() */
    mp_js_do_str(QueryCode.join('\n'));
    wrap_equal("ATTR", JSON.stringify(Getter)); //Add ATTR to mpy, ATTR is common getter

    /*Setup lv_task_handler loop*/
    var the_mp_handle_pending = Module.cwrap('mp_handle_pending', null, [], { async: true });
    function handle_pending() {
        the_mp_handle_pending();
        setTimeout(handle_pending, 10); // should call lv_task_handler()
    }

    /*Initialize the REPL.*/
    mp_js_init_repl();

    /*Start the main loop, asynchronously.*/
    handle_pending();
}

// Init the ace editor
const editorInit = (vm) => {
    let editor = ace.edit("code-editor");
    editor.getSession().setUseWrapMode(true);
    editor.setAutoScrollEditorIntoView(true);
    editor.setFontSize(15);
    editor.resize();
    let c_edit_mode = ace.require("ace/mode/c_cpp").Mode;
    let py_edit_mode = ace.require("ace/mode/python").Mode;
    editor.session.setMode(new py_edit_mode());
    editor.setOptions({maxLines: "200px" });
    vm.editor = editor;
    vm.py_edit_mode = py_edit_mode;
    vm.c_edit_mode = c_edit_mode;
}


const WALV_MAIN = {
    el: "#walv",

    data: {
        version: '1.0.0',
        versionCode: 1,
        editor: null,
        c_edit_mode: null,
        py_edit_mode: null,
        is_c_mode: true, //true: c, false: python
        Term: null,

        buffer: [],
        str_json: "",
        mask: false,
        currentWidget: {},   // The Attributes
        posJSON: {},
        WidgetPool: {},
        InfoPool: {},
        Api: {},

        setter: setter,
        style_setter: [],
        style_base: [],
        currentType: null,

        //Simulator
        screenWidth: 480,
        screenHeight: 320,
        cursorX: 0,
        cursorY: 0,

        //Creator
        creator_options: WidgetsOption,
        props: {emitPath: false, expandTrigger: 'hover'},
        selectedType: "",
        widgetNum: 0,
        Count: 0,
        act_FileName: "",

        //TreeView
        widget_tree: [
            {
                label: "screen",
                children: []
            },
            // For invisible
            {
                label: "",
                children: []
            }
        ],
        // Which node in TreeView was checked
        checkedNode: {
            id: null,
            obj: null,
            // type: null,  // DEPRECATED
        },

        //Terminal
        term_visible: false,

        // Style Editor
        style_visible: false,
        style: {
            body: {
                main_color: null,
                grad_color: null,
            },
            text: {
                color: "#409EFF",
                font: "font_roboto_16",
            },
            image: {

            },
            line: {

            },
        },

        editScreenSize: false,

        // setting dialog
        dialogSettingVisible: false,
        buildScriptPath: window.localStorage.getItem('buildScriptPath') || '',
        runScriptPath: window.localStorage.getItem('runScriptPath') || '',

        // tabs
        activeTab: 'TFT Simulator',
    },


    watch: {
        //Parse string to JSON
        str_json: function() {
            dispatch_data_changed_event();
            try {
                let tmp = JSON.parse(this.str_json);
                if(Object.keys(tmp).length == 3) {
                    this.posJSON = tmp;

                    //Update Postion
                    this.WidgetPool[tmp['id']]['x'] = this.posJSON['x'];
                    this.WidgetPool[tmp['id']]['y'] = this.posJSON['y'];

                    this.changeInfo(tmp['id'], 'x');
                    this.changeInfo(tmp['id'], 'y');

                    // Change the Setting to show the widget that was just moved.
                    this.currentWidget = this.WidgetPool[tmp['id']];
                    this.currentType = this.InfoPool[tmp['id']]['type'];

                   // this.drawRect(this.currentWidget.x, this.currentWidget.y, this.currentWidget.width, this.currentWidget.height);
                } else {
                    if (this.WidgetPool[tmp['id']]) {
                        this.WidgetPool[tmp['id']].x = tmp.x;
                        this.WidgetPool[tmp['id']].y = tmp.y;
                        this.WidgetPool[tmp['id']].width = tmp.width;
                        this.WidgetPool[tmp['id']].height = tmp.height;
                    } else {
                        this.WidgetPool[tmp['id']] = tmp;
                    }
                    this.currentWidget = this.WidgetPool[tmp['id']];
                    this.currentType = this.InfoPool[tmp['id']]?.type;
                }
            } catch (error) {
                alert(error);
            }
        },

    },

    mounted(){
        this.selectScreenTreeNode();

        if (!this.InfoPool['screen']) {
            this.addInfo('screen', '', 'screen');
        }

        let filterOutStyle = [
            "x",
            "y",
            "width",
            "height",
        ]

        //get Base/Other categorize obj's apis
        for (let objKey in style_setter) {
            let styleApis = style_setter[objKey];
            let baseApiObjArray = [];
            let otherApiObjArray = [];
            for (let apiKey in styleApis) {
                let inFilterOut = (filterOutStyle.indexOf(apiKey) != -1)
                if (inFilterOut) {
                    continue;
                }
                let apiObj = styleApis[apiKey];
                if (apiObj.categorize == Categorize.Base) {
                    baseApiObjArray.push(apiObj);
                } else if (apiObj.categorize == Categorize.Other) {
                    otherApiObjArray.push(apiObj);
                }
            }
            if (baseApiObjArray.length > 0) {
                let info = {
                    obj: objKey,
                    apis: baseApiObjArray
                }
                this.style_base.push(info)
            }
            if (otherApiObjArray.length > 0) {
                let info = {
                    obj: objKey,
                    apis: otherApiObjArray
                }
                this.style_setter.push(info)
            }
        }
    },

    methods: {
        // Handle the information(strats with \x06, end with \x15)
        handleOutput: function(text) {

            if(text == '\x15')      //End: '\x15'
            {
                this.mask = false;
                this.str_json = this.buffer.join('');
            }                
            if(this.mask)
            {
                this.buffer.push(text);
                text = "";
            }        
            if(text == '\x06')      //Begin: '\x06'
            {
                this.mask = true;
            }

            if(text == '\n')
            {
                this.buffer.splice(0, this.buffer.length);
            }
            return text;
        },

        Creator: function() {
            if (this.selectedType == "") {
                this.$message({
                    message: 'Please select a type',
                    type: 'error'
                });
                return;
            } else {
                let parent_id = this.getCurrentID();
                if (parent_id === null) {
                    this.$message({
                        message: 'You must choose a widget!',
                        type: 'error'
                    });
                    return;
                }
                if (parent_id == "") {
                    this.$message({
                        message: 'You created a widget invisible',
                        type: 'warning'
                    });

                }
                this.createWidget(this.selectedType, parent_id);
            }
        },

        //Parametres are the String type
        createWidget: function(type, strPar) {
            var id = this.makeID(type);
            var par = strPar;

            wrap_create(id, par, type);

            //TODO: BUG
            this.appendNode(id);

            //** walv saves the inital info to WidgetPool && InfoPool

            //Store Info that a widget was created from.
            this.addInfo(id, par, type);

            // Change currentType, walv will render new input for setters
            this.currentType = type;
        },

        // Increase by 1
        makeID: function(type) {
            let id = type + (this.Count++).toString(16);
            this.widgetNum += 1;
            return id;
        },

        // Append new node to TreeView
        appendNode(widget_name) {
            let new_child = {
                label: widget_name,
                children: [] };
            let node = this.$refs.TreeView.getCurrentNode();
            if (node != null) {
                node.children.push(new_child);
            }
        },

        // Delete node and its childs(reverse)
        deleteNode: function() {
            const node = this.checkedNode.obj;
            const id = this.checkedNode.id;

            if (id == "screen" || id == "") {
                this.$message({
                    message: "You can't delete the screen or nothing!",
                    type: 'error'
                });
                return; // Not support delete screen now
            }
            // delete child
            let record = [id]; // Which child was deleted
            reverse_del_node(node.data, record);

            // delete itself
            const children = node.parent.data.children;
            const index = children.findIndex(d => d.label === id);
            wrap_delete(id);
            children.splice(index, 1);
            this.widgetNum -= record.length;

            // Clear this.checkedNode
            this.checkedNode.obj = null;
            this.checkedNode.id = null;

            // Remove the related info
            pool_delete(this.WidgetPool, record);
            pool_delete(this.InfoPool, record);
            this.currentWidget = this.WidgetPool['screen'];

            this.selectScreenTreeNode();
            dispatch_data_changed_event();

            this.$message({
                message: 'Delete sucessfully',
                type: 'success'
            });
        },

        // When the node is clicked, walv will: change the checkedNode, set new id for label, update the Setting
        // https://element.eleme.cn/#/en-US/component/tree
        clickNode: function(data, obj, tree_obj) {
            this.checkedNode.id = data.label;
            this.checkedNode.obj = obj;

            let id = data.label;
            if (id == "") {// NOTICE
                return;
            }
            // If WidgetPool doesn't has infomation of the widget
            if (this.WidgetPool[id] == undefined) {
                let type = "\'obj\'";
                if (id != "screen") {
                    type = this.InfoPool[id]['type'];
                }
                wrap_query_attr(id, type);
                return;
            }
            // if (id != 'screen') {
            //     this.checkedNode.type = this.InfoPool[id]['type'];   // TODO
            // } DEPRECATED
            this.currentWidget = this.WidgetPool[id];
            this.currentType = this.InfoPool[id]?.type;
            if (id === 'screen') {
                this.currentType = 'screen';
            }
        },

        // Update the X & Y below the Simulator
        cursorXY : function(event) {
            const width = this.WidgetPool?.screen?.width;
            const height = this.WidgetPool?.screen?.height;
            if (!width || !height) {
                return;
            }
            this.cursorX = 0 | (event.offsetX / (600/width));
            this.cursorY = 0 | (event.offsetY / (400/height));
        },

        // Get the id of recently checked node
        getCurrentID: function() {
            return this.checkedNode.id;
            // node = this.$refs.TreeView.getCurrentNode()
            // if (node != null) {
            //     return node.label;
            // }
            // return null;
        },

        // Lock the widget, so it can't move anymore
        // lock_widget: function() {
        //     let drag_state = this.currentWidget["get_drag"];
        //     if(drag_state == true) {
        //         drag_state = "True";
        //     } else {
        //         drag_state = "False";
        //     }

        //     mp_js_do_str(this.currentWidget["id"] + ".set_drag(" + drag_state + ')');
        // },


        // Apply change to the widget: number
        bindWidgetNumerical: function(attribute) {
            dispatch_data_changed_event();

            let value = this.currentWidget[attribute];

            if(value == null) {
                value = 0;
            }

            let id = this.currentWidget["id"];

            wrap_simple_setter(id, attribute, value);

            this.changeInfo(id, attribute);
        },

        // Apply change to the widget: boolean
        bindWidgetBool: function(attribute) {

            let value = this.currentWidget[attribute];

            if(value == true) {
                value = "True"
            } else {
                value = "False"
            }

            let id = this.currentWidget["id"];

            wrap_simple_setter(id, attribute, value);

            this.reverseInfo(id, attribute);
        },

        bindWidgetSpecial: function(e, f) {
            let id = this.currentWidget["id"];
            let api = f['api'];
            let params = e.target.value;
            if(params == "") {  // If input nothing
                return;
            }
            wrap_setter_str(id, api, params);
        },

        // Add some information for the new widget to InfoPool
        addInfo: function(id, par_name, type) {
            let info = {
                type: type,
                parent: par_name,
                cb: false,
                attributes: ["x", "y", "width", "height"],
                apis:[],
                styles:[],
            };
            this.InfoPool[id] = info;
        },

        // For text or number, save something in InfoPool
        changeInfo: function(id, attribute_name) {
            if (id == 'screen' && this.InfoPool[id] == undefined) {
                this.addInfo('screen', '', 'screen');
            }
            let index = this.InfoPool[id].attributes.indexOf(attribute_name);
            if (index == -1) {
                this.InfoPool[id].attributes.push(attribute_name);
            }
        },

        // For boolean only, save something in InfoPool
        reverseInfo: function(id, attribute_name) {
            let index = this.InfoPool[id].attributes.indexOf(attribute_name);
            if (index != -1) {
                this.InfoPool[id].attributes.splice(index, 1);
            } else {
                this.InfoPool[id].attributes.push(attribute_name);
            }
        },

        // User enable CallBack template
        enableCBInfo: function(id) {
            dispatch_data_changed_event();
            this.InfoPool[id].cb = !this.InfoPool[id].cb;
        },

        refreshTerm: function() {
            this.Term.clear();
            this.Term.write("\r\x1b[K>>> ");
        },

        // Take a screenshot for the Simulator
        screenshot: function() {
            document.getElementById("canvas").toBlob((blob) => {
                saveAs(blob, "screenshot.png");
            });
        },

        // Generate the code and print them to the editor.
        generateCode: async function() {
            let preview_code = "";
            if (this.is_c_mode) {
                preview_code = c_generator(this.InfoPool, this.WidgetPool, this.act_FileName);
            } else {
                preview_code = python_generator(this.InfoPool, this.WidgetPool, this.act_FileName);
            }
            await this.editor.setValue(preview_code);
            this.activeTab = 'Code Editor';
            this.$message({
                message: 'Generate code sucessfully',
                type: 'success'
            });
        },

        // Export the code in editor as a file.
        exportCodeAsFile: async function() {
            await this.generateCode();
            let code = await this.editor.getValue();
            // this.$message({
            //     message: 'Export file sucessfully',
            //     type: 'success'
            // });
            // let blob = new Blob([code], {type: "text/plain;charset=utf-8"});
            let fileName = this.act_FileName;
            if (this.is_c_mode) {
                // saveAs(blob, "lv_gui.h");
                this.sendMessage(CMD.exportCodeAsFile, {name: `lty_lv_${fileName}.h`, code});
            } else {
                // saveAs(blob, "lv_gui.py");
                this.sendMessage(CMD.exportCodeAsFile, {name: `lty_lv_${fileName}.py`, code});
            }
        },

        // Set the style
        makeStyle: function() {
            wrap_simple_style(this.currentWidget["id"], this.style);
        },

        //Highlight object
        drawRect: (x, y, w, h) => {
            let ctx = document.getElementById("canvas").getContext("2d");
            ctx.strokeStyle="blue";
            ctx.lineWidth = 2;
            ctx.setLineDash([5,5]);
            ctx.strokeRect(x, y, w, h);
        },

        setArgs: (args) => {
            return setArgvs(args);
        },

        sendMessage(cmd, data) {
            // axios.post(`/index`, {cmd, data}).then(res => {
            //     // console.log(`状态码: ${res.statusCode}`)
            //     // console.log("client receive:", res.data)
            // }).catch(error => {
            //     console.error(error);
            // });

            return axios.post(`/index`, {cmd, data}).catch(error => {
                console.error(error);
            });
        },

        changeScreenSize() {
            this.WidgetPool.screen.height = this.screenHeight;
            this.WidgetPool.screen.width = this.screenWidth;

            if (this.savePage({}, MSG_SAVE_PAGE_SUCC)) {
                window.location.reload();
            } else {
                this.$message({
                    message: 'Change failed',
                    type: 'error'
                });
            }
        },

        selectBuildScript() {
            this.sendMessage(CMD.selectScript).then(v => {
                if (v && v.status === 200 && v.data) {
                    this.buildScriptPath = v.data;
                    this.saveBuildScript();
                }
            });
        },

        selectRunScript() {
            this.sendMessage(CMD.selectScript).then(v => {
                if (v && v.status === 200 && v.data) {
                    this.runScriptPath = v.data;
                    this.saveRunScript();
                }
            });
        },

        build() {
            if (this.buildScriptPath) {
                this.sendMessage(CMD.build, this.buildScriptPath);
            } else {
                this.dialogSettingVisible = true;
            }
        },

        run() {
            if (this.runScriptPath) {
                this.sendMessage(CMD.run, this.runScriptPath);
            } else {
                this.dialogSettingVisible = true;
            }
        },

        saveBuildScript() {
            window.localStorage.setItem('buildScriptPath', this.buildScriptPath);
        },

        saveRunScript() {
            window.localStorage.setItem('runScriptPath', this.runScriptPath);
        },

        // Save code to lty file.
        savePage: async function (event, msg = MSG_SAVE_PAGE_SUCC) {
            let lty_data = {};
            lty_data[`version`] = this.version;
            lty_data[`versionCode`] = this.versionCode;
            lty_data[`InfoPool`] = this.InfoPool;
            lty_data[`WidgetPool`] = this.WidgetPool;
            lty_data[`widget_tree`] = this.widget_tree;
            lty_data[`selectedType`] = this.selectedType;
            lty_data[`widgetNum`] = this.widgetNum;
            lty_data[`Count`] = this.Count;
            lty_data[`term_visible`] = this.term_visible;
            lty_data[`mask`] = this.mask;
            lty_data[`is_c_mode`] = this.is_c_mode;
            
            const res = await this.sendMessage(CMD.savePage, lty_data);
            if (res.data) {
                this.$message({
                    message: msg,
                    type: 'success'
                });
            } else {
                this.$message({
                    message: 'Save failed',
                    type: 'error'
                });
            }
            return res.data;
        },

        selectScreenTreeNode() {
            this.$nextTick(() => {
                this.$refs.TreeView.$children.forEach(element => {
                    element.$props.node.label === "screen" && element.$el.click();
                });
            });
        },
    },
}

window.addEventListener('beforeunload', (event) => {
    event.preventDefault();
    event.returnValue = '';
});