const reverse_del_node = (node, record) => {
    let childs = node.children;
    for (const iter of childs) {
        reverse_del_node(iter, record);
        wrap_delete(iter.label);
        record.push(iter.label);
    }
    childs.splice(0, childs.length);
}

const pool_delete = (pool, list) => {
    for (const i of list) {
        delete pool[i];
    }
}

const dispatch_data_changed_event = () => {
    let event = new Event(Data_Changed_Event)
    window.dispatchEvent(event);
}

let timeoutID;
const debounceFun = function (delay, callback) {
  function wrapper() {
    const self = this;
    const args = arguments;

    function exec() {
      callback.apply(self, args);
    }

    clearTimeout(timeoutID);
    timeoutID = setTimeout(exec, delay);
  }

  return wrapper();
};

// arguments
const setArgvs = (args) => {
    let args_list = [];
    for (const i of args) {
        args_list.push(i["name"])
    }
    return args_list.toString();
}

Vue.component('lvgl-setter', {
    props: ['id', 'name', 'body', 'infpool', 'widgpool'],
    data: function() {
        return {
            args: [],
        }
    },
    methods: {
        checkArgs: function() {
            dispatch_data_changed_event();

            let args_list = [];
            for (const arg of this.args) {

                // if (arg['value'] === "") {
                //     continue
                // }
                if (arg['type'] === "char*") {
                    args_list.push(`"${arg['value']}"`);
                } else if (arg['type'] === "bool") {
                    if (arg['value'] === true) {
                        args_list.push("True");
                    } else {
                        args_list.push("False");
                    } 
                } else {
                    args_list.push(arg.value);
                }
                // TODO: We can check each value's type here
            }

            let index = this.infpool[this.id].apis.indexOf(this.body.api);
            if (index == -1) {
                this.infpool[this.id].apis.push(this.body.api);
            }
            this.widgpool[this.id][this.body.api] = args_list.toString();
            wrap_setter_str(this.id, this.body.api, args_list.toString());
        },
    },
    // TODO: Rewrite created && beforeUpdate
    created() {
        for (const arg of this.body.args) {
            arg['value'] = "";      // args: [{"name": '', "type": '', "value": ''}]
            if (this.widgpool && this.widgpool[this.id] && this.widgpool[this.id][this.body.api]) {
                let val = this.widgpool[this.id][this.body.api];
                if (arg['type'] === "char*") {
                    arg['value'] = val.slice(1, val.length - 1);
                } else if (arg['type'] === "bool") {
                    arg['value'] = val === "True" ? true : false;
                } else {
                    arg['value'] = val;
                }
            }
            this.args.push(arg);
        }
    },
    // Why we need beforeUpdate: https://vuejs.org/v2/guide/list.html#Maintaining-State. If template was rendered by other, Vue won't excute created but beforeUpdate
    beforeUpdate() {
        this.args.splice(0, this.args.length);
        for (const arg of this.body.args) {
            arg['value'] = "";
            this.args.push(arg);
        }
    },
    template: '<span> {{ name }} (<small v-for="arg in args">{{arg.name}}:<input type="checkbox" v-if="arg.type === `bool`" v-model="arg.value" v-on:change="checkArgs()"/> <input type="text" style="width: 35px" v-else v-model="arg.value" v-bind:placeholder="arg.type" v-on:input="checkArgs()"/>,</small>)</span>'
});

Vue.component('lvgl-style-setter', {
    props: ['id', 'name', 'body', 'infpool', 'widgpool'],
    data: function() {
        return {
            args: [],
        }
    },
    methods: {
        updateArgs: function() {
            let args_list = [];
            for (const arg of this.args) {
                if (arg['value'] === "") {
                    continue
                }
                if (arg['type'] === "char*") {
                    args_list.push(`"${arg['value']}"`);
                } else if (arg['type'] === "bool") {
                    if (arg['value'] === true) {
                        args_list.push("True");
                    } else {
                        args_list.push("False");
                    }
                } else {
                    args_list.push(arg.value);
                }
                // TODO: We can check each value's type here
            }

            let index = this.infpool[this.id].styles.indexOf(this.body.api);
            if (index == -1) {
                this.infpool[this.id].styles.push(this.body.api);
            }

            this.widgpool[this.id][this.body.api] = args_list.toString();
            wrap_style_setter_str(this.id, this.infpool, this.widgpool);
        },
    },
    // TODO: Rewrite created && beforeUpdate
    created() {
        for (const arg of this.body.args) {
            arg['value'] = "";      // args: [{"name": '', "type": '', "value": ''}]
            if (this.widgpool && this.widgpool[this.id] && this.widgpool[this.id][this.body.api]) {
                let val = this.widgpool[this.id][this.body.api];
                if (arg['type'] === "char*") {
                    arg['value'] = val.slice(1, val.length - 1);
                } else if (arg['type'] === "bool") {
                    arg['value'] = val === "True" ? true : false;
                } else {
                    arg['value'] = val;
                }
            }
            this.args.push(arg);
        }
    },
    // Why we need beforeUpdate: https://vuejs.org/v2/guide/list.html#Maintaining-State. If template was rendered by other, Vue won't excute created but beforeUpdate
    beforeUpdate() {
        this.args.splice(0, this.args.length);
        for (const arg of this.body.args) {
            arg['value'] = "";
            this.args.push(arg);
        }
    },
    template: '<span> {{ name }} (<small v-for="arg in args">{{arg.name}}:<input type="checkbox" v-if="arg.type === `bool`" v-model="arg.value" v-on:change="updateArgs()"/> <input type="text" style="width: 35px" v-else v-model="arg.value" v-bind:placeholder="arg.type" v-on:input="updateArgs()"/>,</small>)</span>'
});

Vue.component('lvgl-style-arg-item', {
    props: ['arg_item', 'arg_item_api_name', 'arg_item_infpool', 'arg_item_widgpool', 'arg_item_id'],
    data: function () {
        return {
            args: [],
            value: this.arg_item.value,
            defaultFonts: [
                { label: '14', value: 'lv.font_montserrat_14' },
                { label: '16', value: 'lv.font_montserrat_16' },
            ],
        }
    },
    methods: {
        updateArgs: function () {
            dispatch_data_changed_event();

            let index = this.arg_item_infpool[this.arg_item_id].styles.indexOf(this.arg_item_api_name);
            if (index == -1) {
                this.arg_item_infpool[this.arg_item_id].styles.push(this.arg_item_api_name);
            }

            this.arg_item_widgpool[this.arg_item_id][this.arg_item_api_name] = this.value;
            wrap_style_setter_str(this.arg_item_id, this.arg_item_infpool, this.arg_item_widgpool);
        },
        isPickColorType: function (type) {
            return type === "color32_t";
        },
        isSelectType: function (type) {
            return type === "font_t";
        }
    },
    created() {
    },
    beforeUpdate() {

    },
    template:
        `
    <small>
        {{arg_item.name}}:<el-color-picker size="mini"  v-model="value" v-if="isPickColorType(arg_item.type)" @change="updateArgs()"></el-color-picker>
                          <el-select v-else-if="isSelectType(arg_item.type)" size="mini" v-model="value" @change="updateArgs()">
                                <el-option v-for="item in defaultFonts" :key="item.value" :value="item.value" :label="item.label">
                                </el-option>
                          </el-select>
                          <input type="text" style="width: 35px" v-else v-model="value" v-bind:placeholder="arg_item.type" v-on:input="updateArgs()"/>
    </small>
    `
})

Vue.component('lvgl-style-setter-new', {
    props: ['id', 'body', 'infpool', 'widgpool'],
    data: function() {
        return {
            apis: [],
            args: [],
            color_value:'',
            font_value:'',
            defaultFonts: [
                { label: '14', value: 'lv.font_montserrat_14' },
                { label: '16', value: 'lv.font_montserrat_16' },
            ],
        }
    },
    methods: {
        updateArgs: function (currentApiName) {
            let args_list = [];
            // only update current api's args
            let currentApiArgs = this.apis.filter(v=>v.api === currentApiName)
            if (currentApiArgs <= 0) {
                console.log('currentApiArgs len is 0');
                return;
            }
            for (const arg of currentApiArgs[0].args) {
                if (arg['value'] === "" && this.color_value === "" && this.font_value === "") {
                    continue
                }
                if (arg['type'] === "char*") {
                    args_list.push(`"${arg['value']}"`);
                } else if (arg['type'] === "bool") {
                    if (arg['value'] === true) {
                        args_list.push("True");
                    } else {
                        args_list.push("False");
                    }
                } else if (arg['type'] === "color32_t") {
                    args_list.push(this.color_value)
                } else if (arg['type'] === "font_t") {
                    args_list.push(this.font_value)
                } else {
                    args_list.push(arg.value);
                }
                // TODO: We can check each value's type here
            }

            let index = this.infpool[this.id].styles.indexOf(currentApiName);
            if (index == -1) {
                this.infpool[this.id].styles.push(currentApiName);
            }

            this.widgpool[this.id][currentApiName] = args_list.toString();
            wrap_style_setter_str(this.id, this.infpool, this.widgpool);
        },
        isPickColorType: function (type) {
            return type === "color32_t";
        },        
        isSelectType: function (type) {
            return type === "font_t";
        }
    },
    // TODO: Rewrite created && beforeUpdate
    created() {
        for (const api of this.body.apis) {
            for (const i in api.args) {
                api.args[i]['value'] = "";
                if (this.widgpool && this.widgpool[this.id] && this.widgpool[this.id][api.api]) {
                    let val = this.widgpool[this.id][api.api];
                    if (api.args[i]['type'] === "char*") {
                        api.args[i]['value'] = val.slice(1, val.length - 1);
                    } else if (api.args[i]['type'] === "bool") {
                        api.args[i]['value'] = val === "True" ? true : false;
                    } else {
                        api.args[i]['value'] = val;
                    }
                }
            }
            this.apis.push(api);
        }
    },
    mounted() {
    },
    
    // Why we need beforeUpdate: https://vuejs.org/v2/guide/list.html#Maintaining-State. If template was rendered by other, Vue won't excute created but beforeUpdate
    beforeUpdate() {
  
    },
     template: 
        `
        <div>
            <div v-for="api in apis">
                {{ api.desc==""?api.api:api.desc }}:
                (<small>
                    <small v-for="arg in api.args">
                        <lvgl-style-arg-item v-bind:arg_item="arg" v-bind:arg_item_api_name="api.api" v-bind:arg_item_infpool="infpool" v-bind:arg_item_widgpool="widgpool" v-bind:arg_item_id="id">
                        </lvgl-style-arg-item>
                    </small>
                </small>)
            </div>
        </div>
        `
    //  '<span> {{ name }} (<small v-for="arg in args">{{arg.name}}:<input type="checkbox" v-if="arg.type === `bool`" v-model="arg.value" v-on:change="updateArgs()"/> <input type="text" style="width: 35px" v-else v-model="arg.value" v-bind:placeholder="arg.type" v-on:input="updateArgs()"/>,</small>)</span>'
});


// https://docs.littlevgl.com/en/html/object-types/obj.html
// Vue.component('lvgl-layout', {
//     props: ['id', 'x', 'y', 'align', 'obj_ref', 'x_shift', 'y_shift'],  // lv_obj_set_x, lv_obj_set_y, lv_obj_align(obj, obj_ref, LV_ALIGN_..., x_shift, y_shift)
//     data: function() {
//         return {
//             x: 0,
//             y: 0,
//             align: "", 
//         }
//     },
//     template: ''
// });

// TODO: Only support align to its parent now
Vue.component('lvgl-align', {
    props: ['id', 'ref_id'],
    data: function() {
        return {
            align: '',
            ref_obj: '',
            x_shift: 0,
            y_shift: 0,
            options: [
                '',                 'OUT_TOP_LEFT',     'OUT_TOP_MID',  'OUT_TOP_RIGHT', '', 0, 
                'OUT_LEFT_TOP',     'IN_TOP_LEFT',      'IN_TOP_MID',   'IN_TOP_RIGHT', 'OUT_RIGHT_TOP', 0,
                'OUT_LEFT_MID',     'IN_LEFT_MID',      'CENTER',        'IN_RIGHT_MID', 'OUT_RIGHT_MID', 0,
                'OUT_LEFT_BOTTOM', 'IN_BOTTOM_LEFT',    'IN_BOTTOM_MID', 'IN_BOTTOM_RIGHT', 'OUT_RIGHT_BOTTOM', 0,
                '',                 'OUT_BOTTOM_LEFT', 'OUT_BOTTOM_MID', 'OUT_BOTTOM_RIGHT', '', 0,
            ]
        }
    },
    watch: {
        align: (type) => {
            wrap_align(this.id, 'None', type, 0, 0);
        }
    },
    template: '<div><p>{{align}}</p><span v-for="type in options"><input type="radio" v-bind:disabled="type === ``" v-if="type !== 0" v-bind:value="type" v-model="align"/><br v-else /></span></div>'
});