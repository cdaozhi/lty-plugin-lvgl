/* generate the C or Python code to a file */

function python_generator(info, widget, actFileName) {
    let code = [];

    for (const key in info) {
        let id = key;

        let par_id = info[key].parent;

        let type = info[key].type;

        //screen no need generate create code
        if (type !== 'screen') {
            code.push(template_py_create(id, par_id, type));    //code: create, EX: btn0 = lv.btn(scr)
        }

        if (info[id].cb) {
            code.push(template_py_cb(id));
        }

        //attributes
        const attributes = info[key].attributes;
        for (const attr of attributes) {
            let value = widget[id][attr];
            if (value == true) {
                value = "True";
            } else if (value == false) {
                value = "False";
            }
            code.push(template_py_setter_simple(id, attr, value));
        }

        //apis
        const apis = info[key].apis;
        for (const api of apis) {
            let value = widget[id][api];
            if (value == true) {
                value = "True";
            } else if (value == false) {
                value = "False";
            }
            if (value) {
                code.push(template_py_api_simple(id, api, value));
            }
        }

        //styles
        const styles = info[key].styles;
        for (let index = 0; index < styles.length; index++) {
            let styleApi = styles[index];
            let value = widget[id][styleApi];
            if (index == 0) {
                //begin
                code.push(`${id}_style = lv.style_t()`)
                code.push(`${id}_style.init()`)
            }
            // content
            if (value) {
                code.push(template_py_style_simple(id, styleApi, value));
            }
            if (index == styles.length - 1) {
                //last
                code.push(`${id}.add_style(${id}_style, 0)`);
            }
        }

    }
    return code.join("\n");
}


function c_generator(info, widget, actFileName) {
    let body = [], cb = [];

    // method end
    let methodBeginCode = [
        `void lty_lv_${actFileName}(lv_obj_t *screen)`,
        `{`,
        `   if (screen == NULL)`,
        `   {`,
                `       screen = lv_scr_act();`,
                `       lv_obj_remove_style_all(screen);`,
                `       lv_obj_set_style_bg_opa(screen, LV_OPA_COVER, 0);`,
        `   }`,
        ``,
    ]
    body.push(methodBeginCode.join('\n'))

    for (const key in info) {
        let id = key;

        let par_id = info[key].parent;

        let type = info[key].type;

        if(info[key].cb) {
            cb.push(id);
        }

        //screen no need generate create code
        if (type !== 'screen') {
            body.push(template_c_create(id, par_id, type));    //code: create, EX: btn0 = lv.btn(scr)
        }

        //attributes
        const attributes = info[key].attributes;
        for (const attr of attributes) {
            let value = widget[id][attr];

            body.push(template_c_setter_simple(id, "obj", attr, value));
        }

        //apis
        const apis = info[key].apis;
        for (const api of apis) {
            let value = widget[id][api];
            if (value) {
                body.push(template_c_api_simple(id, type, api, value, par_id));
            }
        }

        //styles
        const styles = info[key].styles;
        for (let index = 0; index < styles.length; index++) {
            let styleApi = styles[index];
            let value = widget[id][styleApi];
            const bindStyleName = `${id}_style`;
            if (index == 0) {
                //begin
                body.push(`    lv_style_t ${bindStyleName};`)
                body.push(`    lv_style_init(&${bindStyleName});`)
            }
            // content
            if (value) {
                body.push(template_c_style_simple(id, styleApi, value, bindStyleName));
            }
            if (index == styles.length - 1) {
                //last
                body.push(`    lv_obj_add_style(${id}, &${bindStyleName}, 0);`);
            }
        }
        if (info[key].cb) {
            body.push(`    lv_obj_add_event_cb(${id}, ${id}_event_cb, LV_EVENT_ALL, NULL);`)
        }

        body.push("");

    }
    // method end
    body.push(`}`)
    let cb_s = [];
    for (const id of cb) {
        cb_s.push(template_c_cb(id));
        cb_s.push("");
    }

    return template_c_all(body.join("\n"), cb_s.join("\n"), actFileName);
}