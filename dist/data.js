ELEMENT.locale(ELEMENT.lang.en) //i18n

const WidgetsOption = [
    {
        value: 'obj',
        label: "Object"            
    },
    {
        value: 'form',
        label: 'Form',
        children: [
            {
                value: 'btn',
                label: "Button"
            },
            {
                value: 'img',
                label: "Image"
            },
            {
                value: 'label',
                label: "Label",
            },
            {
                value: 'switch',
                label: "Switch"            
            },
            {
                value: 'checkbox',
                label: "Checkbox"
            },
            {
                value: 'dropdown',
                label: "Drop-Down List"            
            },
            {
                value: 'roller',
                label: "Roller"
            }, 
            {
                value: 'slider',
                label: "Slider"
            },          
        ],
    },
    {
        value: 'data',
        label: 'Data',
        children: [
            {
                value: 'bar',
                label: "Bar"            
            },
            {
                value: 'meter',
                label: "Meter"
            },
            {
                value: 'led',
                label: "LED"
            }, 
            {
                value: 'chart',
                label: "Chart"
            },
            {
                value: 'arc',
                label: "Arc"
            },
            {
                value: 'calendar',
                label: "Calendar"
            }
        ]
    },
    {
        value: 'layer',
        label: 'Layer',
        children: [
            {
                value: 'msgbox',
                label: "Message box"
            }
        ]
    }

]


//The Python code to Initialize the environment
const EnvInitCode = (width, height) => [
    "import ujson",
    "import lvgl as lv",
    "lv.init()",
    "import SDL",
    `WIDTH = ${width}`,
    `HEIGHT = ${height}`,
    "ZOOM = 0",
    "FULLSCREEN = False",
    "SDL.init(w=WIDTH, h=HEIGHT,fullscreen=FULLSCREEN, auto_refresh=False)",
    /* Register SDL display driver. */
    "disp_buf1 = lv.disp_draw_buf_t()",
    "buf1_1 = bytes(WIDTH*10)",
    "disp_buf1.init(buf1_1, None, len(buf1_1)//4)",
    "disp_drv = lv.disp_drv_t()",
    "disp_drv.init()",
    "disp_drv.draw_buf = disp_buf1",
    "disp_drv.flush_cb = SDL.monitor_flush",
    "disp_drv.hor_res = WIDTH",
    "disp_drv.ver_res = HEIGHT",
    "disp_drv.register()",
    /*Regsiter SDL mouse driver*/
    "indev_drv = lv.indev_drv_t()",
    "indev_drv.init()",
    "indev_drv.type = lv.INDEV_TYPE.POINTER",
    "indev_drv.read_cb = SDL.mouse_read",
    "indev_drv.register()",
    /* Create a screen with a button and a label */
    "screen = lv.obj()",
    /* Load the screen */
    "lv.scr_load(screen)",
    "baseAttr = dir(lv.obj)"
];


/* Define special function for python*/

// old getobjattr() function:
// "def getobjattr(obj,id,type_s):",
// "    d={}",
// "    d['id']=id",
// "    for i in dir(obj):",
// "        if 'get_' in i:",
// "            try:",
// "                ret = eval(id + '.' + i + '()')",
// "                if isinstance(ret, (int,float,str,bool)):",
// "                    d[i] = ret",
// "            except:",
// "                pass",
// "    for i in ATTR:",
// "        d[i]=eval(id+'.'+ATTR[i]+'()')",
// "    print('\x06'+ujson.dumps(d)+'\x15')",

const QueryCode = [
    //Get and send JSON format text
    "def query_attr(obj,id,type_s):",
    "    d={}",
    "    d['id']=id",
    "    for i in ATTR['obj']:",
    "        d[i]=eval(id+'.'+ATTR['obj'][i]+'()')",
    "    print('\x06'+ujson.dumps(d)+'\x15')",
    "def query_xy(obj,id):",
    "    d={'id':id,'x':obj.get_x(),'y':obj.get_y()}",
    "    indev = lv.indev_get_act()",
    "    vect = lv.point_t()",
    "    indev.get_vect(vect)",
    "    x = obj.get_x() + vect.x",
    "    y = obj.get_y() + vect.y",
    "    obj.set_pos(x, y)",
    "    print('\x06'+ujson.dumps(d)+'\x15')",
    //Callback: only for the lv.EVENT.DRAG_END
    "def walv_callback(event,obj,id):",
    "    code = event.get_code()",
    "    query_xy(obj, id)",
    "    if(code == lv.EVENT.PRESSED):",
    "       query_attr(obj,id,0)",
];


const Getter = {
    "obj": {
        "x": "get_x",
        "y": "get_y",
        "width": "get_width",
        "height": "get_height",
    },

    "label": {
        "label": "get_text",
    }

}

const Setter = {
    "obj": {
        "x": "set_x",
        "y": "set_y",
        "width": "set_width",
        "height": "set_height",
        "drag": "set_drag",
        "click": "set_click",
        "hidden": "set_hidden",
        "top": "set_top",
    },
    "btn": {
        "state": "set_state",
        "toggle": "set_toggle",
        "ink_wait_time": "set_ink_wait_time",
        "ink_in_time": "set_ink_in_time",
    },
    "label": {
        "text": "set_text",
    },
    "led": {

    }
}

