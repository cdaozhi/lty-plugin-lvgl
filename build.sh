#!/bin/bash

packVscodePlugin(){
 rm -rf ./out
# npm version patch
 yarn
 vsce package
}

helpInfo(){
 echo "pack     pack vscode plugin(.vsix)"
}

case "$1" in
   "pack")
   packVscodePlugin
   ;;
   *)
   helpInfo
   ;;
esac
